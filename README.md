<table align="center"><tr><td align="center" width="9999">
<img src="src/static/img/nosad.png" align="center" width="250" alt="Project icon">

[https://nosad.se](https://nosad.se) genereras från Markdown och hostas på gitlab.io.

![Personer](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Antal%20personer%20som%20bidrar&query=%24.length&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F21882815%2Frepository%2Fcontributors&style=for-the-badge&logo=gitlab)

</td></tr></table>

# Om

Sajten är en statisk webbsida som autogeneras utifrån wiki.

Sajtstruktur:
1. nosad.se - Startsida
2. nosad.se/workshops - Alla inspelningar
3. nosad.se/tips - En kurerad lista med lästips inför anskaffning.
4. nosad.se/organisationer - De organisationer som har hållt presentationer eller delat material.
5. nosad.se/katalog - De öppna programvaror som används av offentlig sektor

Allt innehåll hämtas från [wikin](https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/), [betrodda användare](https://gitlab.com/open-data-knowledge-sharing/wiki/-/project_members) kan uppdatera sidorna. Om en ny wikisida skapas med namnet "WIKISIDA" är den direkt nåbar på https://nosad.se/WIKISIDA. Filen src/static/site/_redirects styr om sidan ska få annat namn i webbläsaren, exempelvis skulle https://nosad.se/sida kunna länka till https://nosad.se/WIKISIDA.

## Redaktionella förändringar

Om syftet är att enbart ändra innehållet på sajten kan ändringar göras i ett editeringsgränssnitt på GitLab. Läs nedan rutin för hur bidrag går till.

Till editeringssidan:
1. startsidan - https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/home
2. workshops - https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Digital-Workshopserie
3. tips - https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Lista-med-delat-material
4. katalog - https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Programvaror
5. organisation - https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/Organisationer

Förändringar syns inom några minuter på nosad.se. Varje sida på wikin ska innehålla en sektion "bidra till sidan" längst ner.

## Utveckling (lokal dator)

Serva katalogen i valfri webbserver. Inkluderar en Dockerfil för att inte behöva installera en webbserver. Personligen använder jag Podman istället för Docker.

```bash
# Checkout
git clone https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git
cd open-data-knowledge-sharing.gitlab.io
# Bygg, rensa bort förra versionen, och slutligen kör
podman build -t nosad-www . && podman stop nosad-www && podman rm nosad-www && podman run -it -d -p 4000:4000 --name nosad-www nosad-www
# Besök
http://localhost:4000
```

## Kodbidrag

Kodbidrag sker bidrag via en **MergeRequest**.

> Viktigaste är att vara tydlig med att beskriva den förändring man skickar in. Det behöver inte vara svårare än en tydlig rubrik.

**Process för den som vill bidra**

1. Clone repot: $ git clone https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git
3. Skapa en ny bransch: $ git checkout -b cool_new_design_with_awesome_content
2. Gör dina ändringar och testa
3. Pusha ändringen: $ git push origin cool_new_design_with_awesome_content
4. Gitlab kommer svara med en URL som du måste besöka för att skapa en MergeRequest. Besök sidan och se till att inkludera en tydlig beskrivning av ändringen.

**Gokännandeprocess kod**

Genom att skicka in en MergeRequest är det ägaren till repot som får välja att acceptera förändringen eller inte. Det gör att myndigheten som ansvarar för sidan har kontroll på vad som infogas och kan ta ansvar för den publicerade sidan.

Ett bidrag kan enbart ske om du kan svara JA på följande frågor:
1. Licensierar du ditt bidrag i enlighet med MIT-licensen?
2. Om du har utfört arbetet som en del för din arbetsgivare, har du deras tillåtelse att göra bidraget?

## Licens

MIT
