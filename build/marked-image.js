#!/usr/bin/env node

const { marked } = require('marked');
const path = require('path');

const renderer = new marked.Renderer();
renderer.image = function(href, title, text) {
  // const url = href.startsWith('http') ? href : `images/${href}`; // for external links
  // return `<img src="${url}" alt="${text}" title="${title}" />`;

  const imagePath = path.join(__dirname, 'images', href);

  const getBasenameFormUrl = (urlStr) => {
    const url = new URL(urlStr)
    return path.basename(url.pathname)
}

var image = getBasenameFormUrl(href);

var jsonSafeImageNameWithoutDots = image.split('.').join("");

var jsonSafeImageNameWithoutDotsMedium = "/images/I"+image+"-medium.webp";


var fig =
//  `<figure>
//    <div class="image-container" data-large="${href}">
//      <img class="placeholder img-small" src='{{{I${jsonSafeImageNameWithoutDots}webp}}}' />
//    </div>
//  </figure>`;
//  `<figure>
//    <div class="loader">
//      <img class="img2" width="640" height="427" data-large="${href}"></img>
//      <img width="640" height="427" class="frozen" src='{{{I${jsonSafeImageNameWithoutDots}webp}}}' />
//    </div>
//  </figure>`;
//`<figure class="image-container" data-large="${href}">
//    <img class="placeholder show" src='{{{I${jsonSafeImageNameWithoutDots}webp}}}' />
//</figure>`;
//  `<figure class="loader">
//      <img class="img2" data-large="${href}"></img>
//      <img class="frozen" src="{{{I${jsonSafeImageNameWithoutDots}webp}}}" />
//  </figure>`;
//     <img data-large="${jsonSafeImageNameWithoutDotsMedium}" src="{{{I${jsonSafeImageNameWithoutDots}webp}}}" alt="${text}"></img>
  `<figure class="loader">
    <img class="img2" data-large="${jsonSafeImageNameWithoutDotsMedium}" alt="${text}" />
    <img class="frozen" src="{{{I${jsonSafeImageNameWithoutDots}webp}}}" alt="${text}" />
      <figcaption>Mer <a href="credits">om fotografen</a>.</figcaption>
  </figure>
  <noscript>
  <figure>
    <img src="${href}" alt="${text}"></img>
    <figcaption>Mer <a href="credits">om fotografen</a>.</figcaption>
  </figure>
  </noscript>
  `;
//  `<figure>
//  <img src="${href}"
//    alt="A delicious donut"
//    width="640"
//    height="427"
//    loading="lazy"
//    decoding="async"
//    style="background-size: cover;
//            background-image:
//              url({{{I${jsonSafeImageNameWithoutDots}webp}}});">
//    </figure>

  return fig;//`<img src="${imagePath}" src="${href}" alt="${text}" title="${title}" />`; // for local references
};


var stdin = process.openStdin();

var markdown = "";

stdin.on('data', function(chunk) {
  markdown += chunk;
});

stdin.on('end', function() {
  //console.log("DATA:\n" + data + "\nEND DATA");
  //const markdown = `![](https://bilder.se/20230613_110437.jpg)`;
  const html = marked(markdown, { renderer });

  console.log(html);
});
