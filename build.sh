#!/bin/bash

OUT="build"
SRC="src"
PROJECT="33128289"
STAGE=$OUT/"staging"
BUILD_INFO=$OUT/build.txt

# tear down and re-build the output directory
rm -rf $OUT
mkdir $OUT
mkdir $STAGE
mkdir $OUT/time
mkdir $OUT/style


cp $SRC/script/marked-image.js $OUT/marked-image.js

cp $SRC/assets/stilmallar/style.css $OUT/style/custom.css
minify $OUT/style/custom.css > $OUT/style/custom-min.css

cat $OUT/style/custom-min.css | jq -Rs '{"style":.}' > $OUT/style/style.json
curl -s https://gitlab.com/api/v4/projects/$PROJECT/wikis?with_content=true | jq -cr '.[] | .slug, .' | awk 'NR%2{f=var$0;next} {print >f;close(f)}' var="${STAGE}/"

# https://gitlab.com/api/v4/projects/33128289/wikis?with_content=true

# Create hash for inline scripts
#cat $OUT/style/custom-min.css | openssl dgst -binary -sha256 | openssl base64 | jq -Rs '{"hash":.}' > $OUT/csshash.json

#CO2
curl -s "https://api.websitecarbon.com/b?url=https%3A%2F%2Fnosad.se%2F" > $OUT/CO2.json

#Site configuration, brand, logo and links etc
cp $SRC/assets/configuration/site.json $OUT/site.json

FILES="$STAGE/*"
for f in $FILES
do
  echo "md2html (content) - $f"
  # take action on each file. $f store current file name
  #cat "$f" | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' > "$f"-pre
  cat "$f" | jq -cr '.content' | node --require marked $OUT/marked-image.js | jq -Rs '{"content":.}' > "$f"-pre
  cat "$f" | jq -cr '.content' | marked -s | htmlq --attribute src img | grep https >> $OUT/img-current-url.txt

#  jq -s '.[0] * .[1] * .[2] *.[3]' "$f"-pre $OUT/style/style.json $OUT/CO2.json $OUT/site.json | mustache - $SRC/assets/template/index-new.mustache > "$f".html
done

#find $STAGE/ -iname "*-pre" | xargs cat | jq -cr ".content" | htmlq --attribute data-large img >> $OUT/img-current-url.txt
cat $OUT/img-current-url.txt | xargs basename | sort | uniq > $OUT/img-current.txt

mkdir $OUT/images

#if test -f images.tar.gz; then
#  cd $OUT
#  cp ../images.tar.gz . && tar -zxvf images.tar.gz
#  cd ..
#fi



#find $OUT/images/ -iname "*.webp" > $OUT/img-old.txt
#find $OUT/images/*-medium.webp -type f -exec basename {} \; | sort | uniq > $OUT/img-old.txt

#cat $OUT/img-current.txt | xargs -n 1 printf 'I%1$s-medium.webp\n' > $OUT/img-current-map.txt # I%1$s.webp\n

#comm -1 -3 $OUT/img-old.txt $OUT/img-current-map.txt > $OUT/img-download.txt

cat $OUT/img-current-url.txt | sort | uniq | xargs wget --quiet -P $OUT/images/

for f in $OUT/images/*;
do
  IMAGE_SMALL=$OUT/images/"I$(basename $f)".webp
  IMAGE_MEDIUM=$OUT/images/"I$(basename $f)"-medium.webp
  echo "Small image - $IMAGE_SMALL"
  cwebp -quiet -resize 30 0 -q 30 $f -o $IMAGE_SMALL;
  cwebp -quiet -resize 640 0 -q 80 $f -o $IMAGE_MEDIUM;
  rm $f;
done

#cd $OUT
#tar -zcvf ../images.tar.gz images/
#cd ..

cd $OUT/images
FILES="*.webp"
for f in $FILES
do
JSON_SAFE_NAME="${f//./}"
printf '%s' "data:image/webp;base64,$(cat $f | base64 | tr -d '\r\n')" | jq --arg v "${JSON_SAFE_NAME}" -Rs '{($v):.}' >> small-files.txt # "${f%-*}"
done
cat small-files.txt | jq -sc add > small-files2.txt
rm small-files.txt
cd ../..

FILES="$STAGE/*-pre"
for f in $FILES
do
  echo "Template engine (html) - $f"
  # take action on each file. $f store current file name
  #cat "$f" | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' > "$f"-pre
  #cat "$f" | jq -cr '.content' | node --require marked $OUT/marked-image.js | jq -Rs '{"content":.}' > "$f"-pre
  #cat "$f"-pre | htmlq --attribute data-large img | sort | uniq | xargs wget --quiet -P $OUT/images/

  #cat "$f" | jq -Rs '{"content":.}'

  jq -s '.[0] * .[1] * .[2] *.[3]' "$f" $OUT/style/style.json $OUT/CO2.json $OUT/site.json | mustache - $SRC/assets/template/index-new.mustache > "$f".html
  mustache $OUT/images/small-files2.txt "$f".html > "${f%*-pre}".html
  #rm "$f".html
done


#find $STAGE/ -iname "*.html" | xargs cat | htmlq --attribute data-large img | sort | uniq | xargs wget --quiet -P $OUT/images/

#for f in $OUT/images/*; do cwebp -resize 30 0 -q 0 $f -o $OUT/images/"$(basename $f .jpg)".webp; done

#find $OUT/images/ -not -name "*.*" -exec mv {} {}.jpg \;
#for f in $OUT/images/*-small.webp; do echo -n "data:image/webp;base64,$(cat $f | base64 | tr -d '\r\n')" > $f.txt; done
#echo -n "data:image/webp;base64,$(cat tips.webp | base64 | tr -d '\r\n')"


mv $STAGE/*html $OUT
cp -r $SRC/static/site/* $OUT
cp $SRC/static/img/workshop.jpg $OUT/

#cd $OUT && ln -s news.html news && cd ..
#cd $OUT && ln -s Lista-med-delat-material.html tips && cd ..
#cd $OUT && ln -s Digital-Workshopserie.html workshops && cd ..
cd $OUT && ln -s Slutrapport.html index.html && cd ..

gzip -k $OUT/*html

# Alpine don´t recognize -d
rm -r $STAGE
rm -r $OUT/style
#rm $OUT/style/style.json

echo "Build $(gdate +"%Y-%m-%dT%H:%M:%SZ")" > $BUILD_INFO
